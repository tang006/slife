package com.slife.dao;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.slife.entity.SysCompany;
import org.springframework.stereotype.Component;

/**
 * Created by chen on 2017/4/10.
 * <p>
 * Email 122741482@qq.com
 * <p>
 * Describe:  公司信息 dao
 */
public interface SysCompanyDao extends BaseMapper<SysCompany> {


}
