package com.slife.constant;

/**
 * Created by chen on 2017/7/14.
 * <p>
 * Email 122741482@qq.com
 * <p>
 * Describe: 全局常量
 */
public class Global {

    /**
     * 是/否
     */
    public static final String YES = "Y";
    public static final String NO = "N";




    /**
     * 删除标记（Y：正常；N：删除；A：审核；）
     */
    public static final String DEL_FLAG_NORMAL = "Y";
    public static final String DEL_FLAG_DELETE = "N";
    public static final String DEL_FLAG_AUDIT = "A";
}
