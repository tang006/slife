package com.slife.constant;

/**
 * Created by chen on 2017/9/4.
 * <p>
 * Email 122741482@qq.com
 * <p>
 * Describe: 系统的参数设置，后面要放到数据库和缓存中。
 */
public class Setting {
    /**
     * 文件根目录
     */
    public static final String BASEFLODER = "attach";

    /**
     * excel 目录
     */
    public static final String EXCELADDRESS = "excel";

}
